//
//  DateCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 27/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

protocol DateCellDelegate:class {
    func didTapNextButton()
}

class DateCell : UICollectionViewCell
{
    class var reuseIdentifier: String {return String(describing: self)}
    weak var delegate : DateCellDelegate?
    
    let label : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "When?"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    let datePicker : UIDatePicker = {
        let dp = UIDatePicker()
        dp.timeZone = NSTimeZone.local
        dp.datePickerMode = UIDatePicker.Mode.date
        dp.backgroundColor = .clear
        return dp
    }()
    
    lazy var nextButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "next"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleNext), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews()
    {
        addSubview(label)
        label.setConstraintsWith(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: nil, paddingLeft: 8, paddingTop: 8, paddingRight: 8, paddingBottom: 0, height: 25, width: 0)
        addSubview(nextButton)
        nextButton.setConstraintsWith(left: nil, top: topAnchor, right: rightAnchor, bottom: nil, paddingLeft: 0, paddingTop: 8, paddingRight: 8, paddingBottom: 0, height: 23, width: 23)
        addSubview(datePicker)
        datePicker.setConstraintsWith(left: leftAnchor, top: label.bottomAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 16, paddingTop: 16, paddingRight: 16, paddingBottom: 8, height: 0 , width: 0)
    }
    
    @objc func handleNext()
    {
        delegate?.didTapNextButton()
    }
    
    
}
