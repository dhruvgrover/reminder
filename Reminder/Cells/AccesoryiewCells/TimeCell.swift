//
//  TimeCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 27/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

protocol TimeCellDelegate : class
{
    func didTapPreviousButton()
}

class TimeCell : UICollectionViewCell
{
    var selectedDate = Date()
    weak var delegate : TimeCellDelegate?
    class var reuseIdentifier: String {return String(describing: self)}
    
    let label : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "What Time?"
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()
    
    lazy var timePicker : UIDatePicker = {
        let dp = UIDatePicker()
        dp.timeZone = NSTimeZone.local
        dp.datePickerMode = UIDatePicker.Mode.time
        dp.backgroundColor = .clear
        dp.addTarget(self, action: #selector(handleTime), for: .valueChanged)
        return dp
    }()
    
    lazy var previousButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back"), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handlePrevious), for: .touchUpInside)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupViews()
    {
        addSubview(label)
        label.setConstraintsWith(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: nil, paddingLeft: 8, paddingTop: 8, paddingRight: 8, paddingBottom: 0, height: 25, width: 0)
        addSubview(previousButton)
        previousButton.setConstraintsWith(left: leftAnchor, top: label.bottomAnchor, right: nil, bottom: nil, paddingLeft: 8, paddingTop: 8, paddingRight: 0, paddingBottom: 0, height: 23, width: 23)
        addSubview(timePicker)
        timePicker.setConstraintsWith(left: leftAnchor, top: label.bottomAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 16, paddingTop: 16, paddingRight: 16, paddingBottom: 8, height: 0, width: 0)
    }
    
    @objc func handlePrevious()
    {
        delegate?.didTapPreviousButton()
    }
    
    @objc func handleTime()
    {
      selectedDate = timePicker.date
      let userInfo = ["time":selectedDate]
      NotificationCenter.default.post(name: NSNotification.Name("addedTime"), object: nil, userInfo: userInfo)
    }
}
