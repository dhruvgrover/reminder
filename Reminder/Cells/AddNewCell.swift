//
//  AddNewCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 13/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

protocol AddNewCellDelegate : class
{
    func didTapAddButton(cell:AddNewCell)
}

class AddNewCell : UITableViewCell
{
    weak var delegate : AddNewCellDelegate?
    class var reuseIdentifier: String {return String(describing: self)}
    let gradientLayer = CAGradientLayer()
    
    var colors = [Any]()
    let cardView : UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12
        view.layer.masksToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    let inputTextView : UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.textAlignment = .left
        textView.backgroundColor = .clear
        textView.textColor = .black
        textView.layer.masksToBounds = true
        textView.layer.cornerRadius = 12
        textView.textContainerInset = UIEdgeInsets(top: 24, left: 16, bottom: 8, right: 8)
        textView.isUserInteractionEnabled = false
        return textView
    }()
    
    lazy var addButton : UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "addButton")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(handleAddButton), for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.darkBackground
        addSubview(cardView)
        cardView.setConstraintsWith(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 16, paddingTop: 16, paddingRight: 16, paddingBottom: 0, height: 0, width: 0)
       // print("CardView: \(cardView.frame)")
        cardView.addSubview(inputTextView)
        //print(inputTextView.frame)
        inputTextView.setConstraintsWith(left: cardView.leftAnchor, top: cardView.topAnchor, right: cardView.rightAnchor, bottom: cardView.bottomAnchor, paddingLeft: 0, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
        addSubview(addButton)
        addButton.setConstraintsWith(left: nil, top: nil, right: rightAnchor, bottom: nil, paddingLeft: 0, paddingTop: 0, paddingRight: 24, paddingBottom: 0, height: 24, width: 24)
        addButton.centerYAnchor.constraint(equalTo: cardView.centerYAnchor, constant: 0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        gradientLayer.frame = cardView.bounds
        for color in colorsArray
        {
            colors.append(color.cgColor)
        }
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        gradientLayer.locations = [0.2,0.4,0.6,0.8,1.0]
        gradientLayer.colors = colors
        cardView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
//    func setGradient()
//    {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = cardView.bounds
//        for color in colorsArray {
//            colors.append(color.cgColor)
//        }
//        gradientLayer.colors = colors
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
//        print(gradientLayer.frame)
//        cardView.layer.insertSublayer(gradientLayer, at: 0)
//    }
    
    @objc func handleAddButton()
    {
      delegate?.didTapAddButton(cell: self)
    }
    
}

