//
//  AddNewTaskCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 24/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

protocol AddNewTaskCellDelegate:class
{
    func didAddtask(cell : AddNewTaskCell)
}

class AddNewTaskCell : UITableViewCell
{
    weak var delegate : AddNewTaskCellDelegate?
    let calendar = Calendar.current
    let componentsDay : Set<Calendar.Component> = [.day,.month,.year]
    let componentsTime : Set<Calendar.Component> = [.hour,.minute,.second]
    var dayComponents : DateComponents?
    var timeComponents : DateComponents?
    
    let taskButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "pending"), for: .normal)
        button.setImage(UIImage(named: "completed"), for: .selected)
        return button
    }()
    
    let taskTitleView : UITextView = {
        let textView = UITextView()
        textView.backgroundColor = .clear
        return textView
    }()
    
    class var reuseIdentifier: String {return String(describing: self)}
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func commonInit()
    {
       selectionStyle = .none
       backgroundColor = .clear
       taskTitleView.delegate = self
       addSubview(taskButton)
       taskButton.setConstraintsWith(left: leftAnchor, top: nil, right: nil, bottom: nil, paddingLeft: 16, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 30, width: 30)
       taskButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
       addSubview(taskTitleView)
       taskTitleView.setConstraintsWith(left: taskButton.rightAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 8, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
       taskTitleView.keyboardAppearance = .dark
    }
    
    @objc func handleDay(notification : NSNotification)
    {
        guard let userInfo = notification.userInfo as? [String:Any] else {return}
        guard let date = userInfo["date"] as? Date else {return}
        dayComponents = calendar.dateComponents(componentsDay, from: date)
    }
    
    @objc func handleTime(notification : NSNotification)
    {
        guard let userInfo = notification.userInfo as? [String:Any] else {return}
        guard let time = userInfo["time"] as? Date else {return}
        timeComponents = calendar.dateComponents(componentsTime, from: time)
    }
    
}
extension AddNewTaskCell : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            if textView.text != nil && textView.text != ""
            {
                getDetails()
                delegate?.didAddtask(cell: self)
                textView.text = ""
                textView.resignFirstResponder()
            }
            return false
        }
        return true
    }
    
    func getDetails()
    {
        if dayComponents == nil
        {
            dayComponents = calendar.dateComponents(componentsDay, from: Date())
        }
        
        if timeComponents == nil
        {
            timeComponents = calendar.dateComponents(componentsTime, from: Date())
        }
        
        print(dayComponents)
        print(timeComponents)
    }
    
}
