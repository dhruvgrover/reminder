//
//  ColourfulCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 07/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

class ColourfulCell: UITableViewCell {
    
    let cellLabel:UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    let cardView:UIView = {
        let view = UIView()
        return view
    }()
    
    class var reuseIdentifier: String{return String(describing: self)}

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    fileprivate func setupCell() {
        self.selectionStyle = .none
        self.contentView.backgroundColor = UIColor.darkBackground
        addSubview(cardView)
        cardView.setConstraintsWith(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 16, paddingTop: 16, paddingRight: 16, paddingBottom: 0, height: 0, width: 0)
        addSubview(cellLabel)
        cellLabel.setConstraintsWith(left: cardView.leftAnchor, top: cardView.topAnchor, right: cardView.rightAnchor, bottom: cardView.bottomAnchor, paddingLeft: 16, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
        cardView.layer.cornerRadius = 12
        cardView.layer.masksToBounds = true
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
