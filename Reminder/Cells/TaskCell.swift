//
//  TaskCell.swift
//  Reminder
//
//  Created by Dhruv Grover on 23/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

class TaskCell : UITableViewCell
{
    
    class var reuseIdentifier: String {return String(describing: self)}
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let taskButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "pending"), for: .normal)
        return button
    }()
    
    let taskLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 16)
        return label
    }()
    
    fileprivate func setupViews()
    {
        backgroundColor = .clear
        addSubview(taskButton)
        taskButton.setConstraintsWith(left: leftAnchor, top: nil, right: nil, bottom: nil, paddingLeft: 16, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 30, width: 30)
        taskButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        addSubview(taskLabel)
        taskLabel.setConstraintsWith(left: taskButton.rightAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 8, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
    }
    
}
