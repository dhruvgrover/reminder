//
//  Constants.swift
//  Reminder
//
//  Created by Dhruv Grover on 13/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

let colorsArray = [UIColor.sunsetOrange,UIColor.sunGlow,UIColor.cyanCornFlowerBlue,UIColor.darkLavender,UIColor.yellowGreen]

class Helpers : NSObject
{
    func getDateFormatter() -> DateFormatter
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy"
        return dateformatter
    }
    
    func getTimeFormatter() -> DateFormatter
    {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "HH:mm:ss"
        return dateFormatter2
    }

}
