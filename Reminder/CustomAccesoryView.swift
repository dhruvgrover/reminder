//
//  CustomAccesoryView.swift
//  Reminder
//
//  Created by Dhruv Grover on 24/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

class CustomAccesoryView: UIView
{
    var datePickerColor : UIColor?
    let calendar = Calendar.current
    var selectedDate = Date()
    var selectedTime = Date()

    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.isScrollEnabled = false
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .init(white: 0, alpha: 0.55)
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 8
        self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        addSubview(collectionView)
        collectionView.register(DateCell.self, forCellWithReuseIdentifier: DateCell.reuseIdentifier)
        collectionView.register(TimeCell.self, forCellWithReuseIdentifier: TimeCell.reuseIdentifier)
        collectionView.setConstraintsWith(left: leftAnchor, top: topAnchor, right: rightAnchor, bottom: bottomAnchor, paddingLeft: 0, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CustomAccesoryView : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,DateCellDelegate, TimeCellDelegate
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item
        {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DateCell.reuseIdentifier, for: indexPath) as! DateCell
            cell.datePicker.setValue(datePickerColor, forKey: "textColor")
            cell.datePicker.addTarget(self, action: #selector(handleDatePicker), for: .valueChanged)
            cell.delegate = self
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeCell.reuseIdentifier, for: indexPath) as! TimeCell
            cell.timePicker.setValue(datePickerColor, forKey: "textColor")
            cell.timePicker.addTarget(self, action: #selector(handleTime), for: .valueChanged)
            cell.delegate = self
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 100)
    }
    
    func didTapNextButton() {
        collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .right, animated: true)
    }
    
    func didTapPreviousButton() {
        collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left
            , animated: true)
    }
    
    @objc func handleDatePicker(sender:UIDatePicker)
    {
        selectedDate = sender.date
    }
    
    @objc func handleTime(sender:UIDatePicker)
    {
        selectedTime = sender.date
    }

        
}
