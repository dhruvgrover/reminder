//
//  DetailViewController.swift
//  Reminder
//
//  Created by Dhruv Grover on 07/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit
import RealmSwift
import UserNotifications

protocol DetailViewControllerDelegate : class {
    func didGoBack()
}

class DetailViewController : UIViewController
{
    weak var delegate : DetailViewControllerDelegate?
    var color : UIColor?
    var rowNumber = 0
    var category : Category?
    var dismissButtonY : NSLayoutConstraint?
    var realm : Realm?
    var tasks = List<Task>()
    var calendar = Calendar.current
    let componentsDay : Set<Calendar.Component> = [.day,.month,.year]
    let componentsTime : Set<Calendar.Component> = [.hour,.minute,.second]

    let label : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .white
        return label
    }()
    
    let dismissButton : UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "close")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        return button
    }()
    
    lazy var taskTableview : UITableView = {
        let tableview = UITableView()
        tableview.backgroundColor = .clear
        return tableview
    }()
    
    lazy var inputAccView : CustomAccesoryView = {
        let view = CustomAccesoryView()
        view.datePickerColor = color
        view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.locale = Locale.current
        view.backgroundColor = .clear
        setupTableView()
        setupDismissButton()
        fetchList()
    }
    
    
    func setupTableView()
    {
      taskTableview.register(TaskCell.self, forCellReuseIdentifier: TaskCell.reuseIdentifier)
      taskTableview.register(AddNewTaskCell.self, forCellReuseIdentifier: AddNewTaskCell.reuseIdentifier)
      taskTableview.separatorStyle = .none
      view.addSubview(taskTableview)
      taskTableview.delegate = self
      taskTableview.dataSource = self
      taskTableview.setConstraintsWith(left: view.leftAnchor, top: nil, right: view.rightAnchor, bottom: view.bottomAnchor, paddingLeft: 0, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
      taskTableview.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.9).isActive = true
    }
        
    func fetchList()
    {
        guard let selectedCategory = category else { return }
        tasks = selectedCategory.tasks
        DispatchQueue.main.async {
            self.taskTableview.reloadData()
        }
    }
    
    @objc func dismissVC()
    {
      delegate?.didGoBack()
      dismiss(animated: true, completion: nil)
    }
    
    func setupDismissButton()
    {
        view.addSubview(dismissButton)
        dismissButtonY = dismissButton.topAnchor.constraint(equalTo: view.topAnchor, constant: -20)
        dismissButtonY?.isActive = true
        dismissButton.setConstraintsWith(left: nil, top: nil, right: view.rightAnchor, bottom: nil, paddingLeft: 0, paddingTop: 0, paddingRight: 16, paddingBottom: 0, height: 23, width: 23)
        UIView.animate(withDuration: 2, delay: 2, options: .curveEaseOut, animations: {
            self.dismissButtonY?.constant = 32
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}

extension DetailViewController : UITableViewDataSource,UITableViewDelegate,AddNewTaskCellDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != tasks.count
        {
            let cell = taskTableview.dequeueReusableCell(withIdentifier: TaskCell.reuseIdentifier) as! TaskCell
            cell.taskLabel.text = tasks[indexPath.row].taskName
            return cell
        }
        else
        {
            let cell = taskTableview.dequeueReusableCell(withIdentifier: AddNewTaskCell.reuseIdentifier) as! AddNewTaskCell
            cell.taskTitleView.inputAccessoryView = inputAccView
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func didAddtask(cell : AddNewTaskCell) {
        let inputAccView = cell.taskTitleView.inputAccessoryView as! CustomAccesoryView
        let dayComp = calendar.dateComponents(componentsDay, from: inputAccView.selectedDate)
        let timeComp = calendar.dateComponents(componentsTime, from: inputAccView.selectedTime)
        var components = DateComponents()
        components.year = dayComp.year
        components.month = dayComp.month
        components.day = dayComp.day
        components.hour = timeComp.hour
        components.minute = timeComp.minute
        components.second = timeComp.second
        guard let createdAt = calendar.date(from: components) else {return}
        guard let taskName = cell.taskTitleView.text else {return}

        let task = Task()
        let id = UUID().uuidString
        task.taskName = taskName
        task.remindAt = createdAt
        task.id = id
        
        realm = try! Realm()
        try! realm?.write {
            tasks.append(task)
            category?.tasks = tasks
        }
        scheduleNotification(forTask: task)
        taskTableview.reloadData()
    }
    
    fileprivate func scheduleNotification(forTask task : Task)
    {
       let content = UNMutableNotificationContent()
       content.title = "Reminder"
       content.body = task.taskName
       content.sound = .default
       let remindDate = task.remindAt
       let dateComponents = calendar.dateComponents([.year,.month,.day,.hour,.minute,.second], from: remindDate)
       let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
       let request = UNNotificationRequest(identifier: task.id, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
}
