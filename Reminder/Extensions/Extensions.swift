//
//  ViewControllerExtensions.swift
//  Reminder
//
//  Created by Dhruv Grover on 07/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

extension UIView
{
    func setConstraintsWith(left:NSLayoutXAxisAnchor?, top:NSLayoutYAxisAnchor?, right:NSLayoutXAxisAnchor?, bottom:NSLayoutYAxisAnchor?, paddingLeft:CGFloat, paddingTop:CGFloat, paddingRight:CGFloat, paddingBottom:CGFloat, height:CGFloat, width:CGFloat)
    {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        
        if height != 0
        {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
        if width != 0
        {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
    }
    
}

extension UIColor
{
    static let sunsetOrange = UIColor(red: 255/255, green: 89/255, blue: 94/255, alpha: 1)
    static let sunGlow = UIColor(red: 255/255, green: 202/255, blue: 58/255, alpha: 1)
    static let yellowGreen = UIColor(red: 138/255, green: 201/255, blue: 38/255, alpha: 1)
    static let cyanCornFlowerBlue = UIColor(red: 25/255, green: 130/255, blue: 196/255, alpha: 1)
    static let darkLavender = UIColor(red: 106/255, green: 76/255, blue: 147/255, alpha: 1)
    static let darkBackground = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
}

extension UIView
{
//    func addGradient(withColors colorsArray : [Any])
//    {
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.frame = self.bounds
//        gradientLayer.colors = colorsArray
////        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
////        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
//        self.layer.insertSublayer(gradientLayer, at: 0)
//    }

}

