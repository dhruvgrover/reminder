//
//  ViewController.swift
//  Reminder
//
//  Created by Dhruv Grover on 07/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit
import RealmSwift

class HomeController: UIViewController {
    
    var zoomtransitionManager : ZoomTransitionManager?
    var realm : Realm?
    var categories : Results<Category>?
    var selectedCategory : Category?
    let colors = colorsArray 
    var keyBoardHeight:CGFloat?
    var oldFrame:CGRect?
    let tableView : UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyBoardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        self.tableView.backgroundColor = UIColor.darkBackground
        setupTableview()
        realm =  try! Realm()
        getAllCategories()
        
    }
    fileprivate func getAllCategories()
    {
        categories = realm?.objects(Category.self)
        print(Realm.Configuration.defaultConfiguration.fileURL)
        tableView.reloadData()
    }
    
    fileprivate func setupTableview()
    {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(ColourfulCell.self, forCellReuseIdentifier: ColourfulCell.reuseIdentifier)
        tableView.register(AddNewCell.self, forCellReuseIdentifier: AddNewCell.reuseIdentifier)
        view.addSubview(tableView)
        tableView.setConstraintsWith(left: view.leftAnchor, top: view.topAnchor, right: view.rightAnchor, bottom: view.bottomAnchor, paddingLeft: 0, paddingTop: 0, paddingRight: 0, paddingBottom: 0, height: 0, width: 0)
    }
    
    @objc func keyBoardWillShow(notification : NSNotification)
    {
        if let keyBoardFrame:NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        {
            keyBoardHeight = keyBoardFrame.cgRectValue.height
        }
    }

}

extension HomeController : UITableViewDelegate, UITableViewDataSource, DetailViewControllerDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = categories?.count else {return 1}
        if count < 5 {
            return count + 1
        }else {
            return count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != categories?.count
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: ColourfulCell.reuseIdentifier) as! ColourfulCell
            cell.cardView.backgroundColor = colors[indexPath.row]
            cell.cellLabel.text = categories?[indexPath.row].categoryName
            return cell
        }
        else
        {
           let cell = tableView.dequeueReusableCell(withIdentifier: AddNewCell.reuseIdentifier) as! AddNewCell
            cell.inputTextView.delegate = self
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .default, title: nil) { (action, indexpath) in
            guard let category = self.categories?[indexPath.row] else {return}
            try! self.realm?.write {
                self.realm?.delete(category)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
        action.backgroundColor = .darkBackground
        return [action]
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete
//        {
//
//           tableView.reloadData()
//        }
//
//    }
    
  //Works after some scrolling
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        guard let cell = cell as? AddNewCell else {return}
//        print(cell.cardView.frame)
//        cell.setGradient()
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? ColourfulCell {
            selectedCategory = categories?[indexPath.row]
            let cardViewFrame  = cell.cardView.superview?.convert(cell.cardView.frame, to: nil)
            let copyOfCardView = UIView(frame: cardViewFrame!)
            copyOfCardView.layer.cornerRadius = 12
            copyOfCardView.layer.masksToBounds = true
            copyOfCardView.backgroundColor = colors[indexPath.row]
            view.addSubview(copyOfCardView)
            zoomtransitionManager = ZoomTransitionManager(cardView: copyOfCardView, cardViewFrame: cardViewFrame!)
            UIView.animate(withDuration: 0.4, animations: {
                copyOfCardView.frame = self.view.frame
                copyOfCardView.layer.cornerRadius = 0
            }) { (expanded) in
                self.gotoDetailsVC(row: indexPath.row)
            }
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? AddNewCell {
            didTapAddButton(cell: cell)
        }
        
   }

    
    func gotoDetailsVC(row: Int)
    {
        let detailVC = DetailViewController()
        detailVC.rowNumber = row
        detailVC.modalTransitionStyle = .crossDissolve
        detailVC.modalPresentationStyle = .overCurrentContext
        detailVC.delegate = self
        detailVC.category = selectedCategory
        detailVC.color = colors[row]
        present(detailVC, animated: true, completion: nil)
    }
    
    func didGoBack() {
        UIView.animate(withDuration: 0.4, animations: {
            self.zoomtransitionManager?.cardView.frame = self.zoomtransitionManager!.cardViewFrame
            self.zoomtransitionManager?.cardView.layer.cornerRadius = 12
        }) { (completed) in
            self.zoomtransitionManager?.cardView.removeFromSuperview()
        }
    }
    
}

extension HomeController : UITextViewDelegate,AddNewCellDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            guard let count = categories?.count else {return false}
            guard let cell = tableView.cellForRow(at: IndexPath(item: count, section: 0)) as? AddNewCell else {return false}
            guard let oldframe = oldFrame else {return false}
            UIView.animate(withDuration: 1.2, animations: {
                cell.frame = oldframe
                cell.inputTextView.isUserInteractionEnabled = false
            }, completion: { (success) in
                cell.addButton.isHidden = false
            })
            
            if cell.inputTextView.text != ""
            {
                let newCategory = Category()
                newCategory.categoryName = cell.inputTextView.text
                cell.inputTextView.text = nil
                try! realm = Realm()
                try! realm?.write {
                    realm?.add(newCategory)
                }
                tableView.reloadData()
            }
            return false
        }
        return true
    }
    
    func didTapAddButton(cell: AddNewCell) {
        cell.inputTextView.becomeFirstResponder()
        guard let height = keyBoardHeight else {return}
        print(height)
        oldFrame = cell.frame
        let newY = self.view.frame.height - cell.frame.height - height
        UIView.animate(withDuration: 0.8, animations: {
            cell.frame = CGRect(x: 0, y: newY-32, width: cell.frame.width, height: cell.frame.height)
            cell.addButton.isHidden = true
            cell.inputTextView.isUserInteractionEnabled = true
        }, completion: nil)
    }
}


