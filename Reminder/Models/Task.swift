//
//  Task.swift
//  Reminder
//
//  Created by Dhruv Grover on 10/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import Foundation
import RealmSwift


class Category : Object
{
    @objc dynamic var categoryName : String = ""
    var tasks = List<Task>()
}

class Task : Object
{
    @objc dynamic var id : String!
    @objc dynamic var taskName : String = ""
    @objc dynamic var remindAt : Date = Date()
    let category = LinkingObjects(fromType: Category.self, property: "tasks")
}
