//
//  ZoomTransitionManager.swift
//  Reminder
//
//  Created by Dhruv Grover on 07/05/19.
//  Copyright © 2019 Dhruv Grover. All rights reserved.
//

import UIKit

struct ZoomTransitionManager
{
    var cardView : UIView
    var cardViewFrame : CGRect
}
